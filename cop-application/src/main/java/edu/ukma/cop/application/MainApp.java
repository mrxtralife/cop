package edu.ukma.cop.application;

import edu.ukma.cop.application.classes.GreetingService;
import edu.ukma.cop.application.classes.Interceptor;
import edu.ukma.cop.application.classes.Transport;
import edu.ukma.cop.application.util.ObjectInfo;
import edu.ukma.cop.framework.annotation.Autowiring;
import edu.ukma.cop.framework.core.BeanFactory;
import edu.ukma.cop.framework.core.GenericXmlApplicationContext;

public class MainApp {

//  private static GenericXmlApplicationContext context = new GenericXmlApplicationContext(
//      MainApp.class);

  //@Autowiring("java.lang.String") /* <- throws 'Class specified in annotation is not compatible' exception*/
  @Autowiring("edu.ukma.cop.application.classes.LowerCasingInterceptor")
  //@Autowiring
  private static Interceptor activeInterceptor;

  public static void main(String[] args) {
    final GenericXmlApplicationContext context = new GenericXmlApplicationContext(MainApp.class);

    BeanFactory factory = context.getBeanFactory();

    GreetingService greetingService =
        factory.getBean("greetingService", GreetingService.class);
    System.out.println(greetingService.getMessage());

    Transport bus =
        factory.getBean("bus", Transport.class);
    bus.getTransport();

    Transport bus2 =
        factory.getBean("bus2", Transport.class);
    bus2.getTransport();

    Transport car =
        factory.getBean("car", Transport.class);
    System.out.println(car.toString());

    Transport enginedBus =
        factory.getBean("enginedBus", Transport.class);
    enginedBus.getTransport();

    Transport enginedCar =
        factory.getBean("enginedCar", Transport.class);
    enginedCar.getTransport();

    Transport enginedPlane =
        factory.getBean("enginedPlane", Transport.class);
    enginedPlane.getTransport();

    //================
    //================ REFLECTION API DEMO
    //================
    System.out.println();
    System.out.println("================ REFLECTION API DEMO ================");
    System.out.println();

    System.out.println(new ObjectInfo(bus, "bus").toString());
    System.out.println("==========");
//    System.out.println(new ObjectInfo(bus.toString(),
//        "String representation of the 'bus' object").toString());

    //================
    //================ CUSTOM INTERCEPTOR AUTOWIRING THROUGH ANNOTATION DEMO
    //================
    //(to prevent recompiling by Eclipse, switch Project > Build Automatically off)
    String output = "Test Interceptor";
    System.out.println("Unintercepted string: " + output);
    System.out.println("Intercepted string: " + activeInterceptor.interceptOutputString(output));

    //This block is needed for being able to inspect currently loaded classes
    //with tools like Java VisualVM
    System.out.println("The end...");
//    try {
//      System.in.read();
//    } catch (IOException e) {
//      e.printStackTrace();
//    }
  }
}