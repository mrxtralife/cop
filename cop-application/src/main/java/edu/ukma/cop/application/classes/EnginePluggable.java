package edu.ukma.cop.application.classes;

public interface EnginePluggable {

  void plugEngine(Engine engine);

}
