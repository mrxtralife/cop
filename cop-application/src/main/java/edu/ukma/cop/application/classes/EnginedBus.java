package edu.ukma.cop.application.classes;

import edu.ukma.cop.framework.annotation.Autowiring;
import edu.ukma.cop.framework.annotation.Qualifier;
import edu.ukma.cop.framework.annotation.Service;

@Service("enginedBus")
public class EnginedBus implements Transport {

  private String message;
  private Engine engine;

  @Autowiring
  public EnginedBus(@Qualifier("engine") Engine engine) {
    this.message = "I'm bus with Engine";
    this.engine = engine;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(final String message) {
    this.message = message;
  }

  public void getTransport() {
    System.out.println(message);
  }
}
