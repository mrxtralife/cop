package edu.ukma.cop.application.classes;

import edu.ukma.cop.framework.annotation.Autowiring;
import edu.ukma.cop.framework.annotation.Qualifier;
import edu.ukma.cop.framework.annotation.Repository;

@Repository
public class EnginedCar implements Transport {

  @Autowiring
  @Qualifier("engine")
  private Engine engine;

  private String name;
  private Integer wheelCount;

  public EnginedCar() {
    this.name = "Engined car";
    this.wheelCount = 4;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Integer getWheelCount() {
    return wheelCount;
  }

  public void setWheelCount(Integer wheelCount) {
    this.wheelCount = wheelCount;
  }

  public String toString() {
    return name + ": " + wheelCount;
  }

  public void getTransport() {
    System.out.println("I am " + name + ". I have " + wheelCount + " wheels.");
  }
}
