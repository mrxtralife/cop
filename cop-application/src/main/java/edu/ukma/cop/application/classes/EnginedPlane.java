package edu.ukma.cop.application.classes;

import edu.ukma.cop.framework.annotation.Autowiring;
import edu.ukma.cop.framework.annotation.Qualifier;
import edu.ukma.cop.framework.annotation.Service;

@Service("enginedPlane")
public class EnginedPlane implements Transport, EnginePluggable {

  private String message;
  private Engine engine;

  public EnginedPlane() {
    this.message = "I'm Plane with Engine";
  }

  @Autowiring
  @Override
  public void plugEngine(@Qualifier("engine") Engine engine) {
    this.engine = engine;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(final String message) {
    this.message = message;
  }

  public void getTransport() {
    System.out.println(message);
  }
}
