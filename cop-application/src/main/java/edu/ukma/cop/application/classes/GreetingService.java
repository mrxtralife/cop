package edu.ukma.cop.application.classes;

public interface GreetingService {

  String getMessage();

  void setMessage(String message);
}
