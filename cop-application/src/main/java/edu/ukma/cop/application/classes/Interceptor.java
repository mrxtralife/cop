package edu.ukma.cop.application.classes;

public interface Interceptor {

  String interceptOutputString(String interceptedString);
}
