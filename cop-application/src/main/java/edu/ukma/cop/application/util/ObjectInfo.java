package edu.ukma.cop.application.util;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Parameter;

public class ObjectInfo {

  private String name = "Anonymous";
  private String className;

  private String superClassName;
  private Class<?>[] implementedInterfaces;
  private Field[] fields;
  private Constructor<?>[] constructors;
  private Method[] methods;

  public ObjectInfo(Object o, String name) {
    if (name != null && !name.isEmpty()) {
      this.name = name;
    }

    this.className = o.getClass().getName();
    this.superClassName = o.getClass().getSuperclass().getName();
    this.implementedInterfaces = o.getClass().getInterfaces();
    this.fields = o.getClass().getDeclaredFields();
    this.constructors = o.getClass().getDeclaredConstructors();
    this.methods = o.getClass().getMethods();
  }

  public String toString() {
    StringBuilder objectInfo = new StringBuilder();
    objectInfo.append("Inspecting ").append(name).append(":\n");
    objectInfo.append("Class name: ").append(className).append("\n");
    objectInfo.append("Parent class: ").append(superClassName).append("\n");
    objectInfo.append("Implemented interfaces:");
    if (implementedInterfaces.length == 0) {
      objectInfo.append(" none\n");
    } else {
      objectInfo.append("\n");
      for (Class<?> ii : implementedInterfaces) {
        objectInfo.append("\t* ").append(ii.getName()).append("\n");
      }
    }

    objectInfo.append("Attributes:\n");
    for (Field f : fields) {
      int fieldModifiers = f.getModifiers();
      objectInfo.append("\t* ");

      if (Modifier.isPublic(fieldModifiers)) {
        objectInfo.append("public ");
      }
      if (Modifier.isProtected(fieldModifiers)) {
        objectInfo.append("protected ");
      }
      if (Modifier.isPrivate(fieldModifiers)) {
        objectInfo.append("private ");
      }
      if (Modifier.isFinal(fieldModifiers)) {
        objectInfo.append("final ");
      }
      if (Modifier.isStatic(fieldModifiers)) {
        objectInfo.append("static ");
      }
      if (Modifier.isSynchronized(fieldModifiers)) {
        objectInfo.append("synchronized ");
      }

      objectInfo.append(f.getType().getSimpleName()).append(" ").append(f.getName()).append(";\n");
    }

    objectInfo.append("Constructors:\n");
    for (Constructor<?> c : constructors) {
      int constructorModifiers = c.getModifiers();
      objectInfo.append("\t* ");

      if (Modifier.isPublic(constructorModifiers)) {
        objectInfo.append("public ");
      }
      if (Modifier.isProtected(constructorModifiers)) {
        objectInfo.append("protected ");
      }
      if (Modifier.isPrivate(constructorModifiers)) {
        objectInfo.append("private ");
      }

      objectInfo.append(c.getName()).append("(");
      if (c.getParameterCount() > 0) {
        for (Parameter p : c.getParameters()) {
          if (Modifier.isFinal(p.getModifiers())) {
            objectInfo.append("final ");
          }
          objectInfo.append(p.getType().getSimpleName());

          if (p.isVarArgs()) {
            objectInfo.delete(objectInfo.length() - 2, objectInfo.length()).append("...");
          }
          objectInfo.append(" ").append(p.getName()).append(", ");
        }
        objectInfo.delete(objectInfo.length() - 2, objectInfo.length());
      }
      objectInfo.append(");\n");
    }

    objectInfo.append("Methods:\n");
    for (Method m : methods) {
      int methodModifiers = m.getModifiers();
      objectInfo.append("\t* ");

      if (Modifier.isPublic(methodModifiers)) {
        objectInfo.append("public ");
      }
      if (Modifier.isProtected(methodModifiers)) {
        objectInfo.append("protected ");
      }
      if (Modifier.isPrivate(methodModifiers)) {
        objectInfo.append("private ");
      }
      if (Modifier.isFinal(methodModifiers)) {
        objectInfo.append("final ");
      }
      if (Modifier.isAbstract(methodModifiers)) {
        objectInfo.append("abstract ");
      }
      if (Modifier.isStatic(methodModifiers)) {
        objectInfo.append("static ");
      }
      if (Modifier.isSynchronized(methodModifiers)) {
        objectInfo.append("synchronized ");
      }

      //In order to get formal method parameters names via reflection, they must be present in .class file.
      //By default Java compiler discards this information,
      //so you'll get arg0, arg1... instead of real informative names.
      //In order to have the method parameters names included in the .class files,
      //you must compile .java files with -parameters compile option.
      //To do this in Eclipse, go to Window > Preferences > Java > Compiler and
      //check the box 'Store information about method parameters (usable via reflection)'
      objectInfo.append(m.getReturnType().getSimpleName()).append(" ").append(m.getName())
          .append("(");
      if (m.getParameterCount() > 0) {
        for (Parameter p : m.getParameters()) {
          if (Modifier.isFinal(p.getModifiers())) {
            objectInfo.append("final ");
          }
          objectInfo.append(p.getType().getSimpleName());

          if (p.isVarArgs()) {
            objectInfo.delete(objectInfo.length() - 2, objectInfo.length()).append("...");
          }
          objectInfo.append(" ").append(p.getName()).append(", ");
        }
        objectInfo.delete(objectInfo.length() - 2, objectInfo.length());
      }
      objectInfo.append(");\n");
    }

    return objectInfo.toString();
  }
}