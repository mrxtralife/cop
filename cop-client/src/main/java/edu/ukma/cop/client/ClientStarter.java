package edu.ukma.cop.client;

import edu.ukma.cop.framework.core.BeanFactory;
import edu.ukma.cop.framework.core.GenericXmlApplicationContext;
import javax.swing.JFrame;

public class ClientStarter {

  /**
   * Runs the client application.
   */
  public static void main(String[] args) throws Exception {
    GenericXmlApplicationContext context = new GenericXmlApplicationContext(ClientStarter.class);
    BeanFactory beanFactory = context.getBeanFactory();

    CapitalizingClient client = beanFactory.getBean("client", CapitalizingClient.class);
    client.getFrame().setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    client.getFrame().pack();
    client.getFrame().setVisible(true);
    client.connectToServer();
  }
}
