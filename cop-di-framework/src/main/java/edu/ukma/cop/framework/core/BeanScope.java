package edu.ukma.cop.framework.core;

public enum BeanScope {
  SINGLETON("singleton"),
  PROTOTYPE("prototype");

  private final String value;

  BeanScope(final String value) {
    this.value = value;
  }

  public static BeanScope fromString(String value) {
    BeanScope[] items = BeanScope.values();
    for (BeanScope item : items) {
      if (item.toString().equalsIgnoreCase(value)) {
        return item;
      }
    }
    return null;
  }

  @Override
  public String toString() {
    return value;
  }
}
