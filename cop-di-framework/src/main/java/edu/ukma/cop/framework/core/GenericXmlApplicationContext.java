package edu.ukma.cop.framework.core;


import static edu.ukma.cop.framework.util.BeanUtils.createComponent;
import static edu.ukma.cop.framework.util.BeanUtils.getComponentName;

import edu.ukma.cop.framework.annotation.Autowiring;
import edu.ukma.cop.framework.annotation.Component;
import edu.ukma.cop.framework.annotation.Controller;
import edu.ukma.cop.framework.annotation.Qualifier;
import edu.ukma.cop.framework.annotation.Repository;
import edu.ukma.cop.framework.annotation.Service;
import edu.ukma.cop.framework.exception.ConfigurationException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class GenericXmlApplicationContext {

  private static final String DEFAULT_CONFIG_FILE_NAME = "cop-di-configuration.xml";

  private final BeanFactory beanFactory;

  public GenericXmlApplicationContext() {
    this(DEFAULT_CONFIG_FILE_NAME);
  }

  public GenericXmlApplicationContext(String configFilename) {
    final URL configResource = getClass().getResource("/" + configFilename);
    if (configResource == null) {
      this.beanFactory = new XmlBeanFactory();
    } else {
      this.beanFactory = new XmlBeanFactory(configResource.getPath(),
          new XmlBeanDefinitionReader());
    }
  }

  public GenericXmlApplicationContext(Class<?> classObject) {
    this(DEFAULT_CONFIG_FILE_NAME);

    final List<Class<?>> componentClasses;
    try {
      componentClasses = scanForComponentClasses(classObject.getClassLoader(),
          classObject.getPackage().getName());
    } catch (Exception e) {
      throw new ConfigurationException("Get classes error", e);
    }

    registerComponentClasses(componentClasses, getBeanFactory());

    injectFields(componentClasses, getBeanFactory());

    injectInterfaces(componentClasses, getBeanFactory());
  }

  private List<Class<?>> scanForComponentClasses(ClassLoader cl, String packageName)
      throws Exception {
    final String packagePath = packageName.replaceAll("[.]", "/");

    List<Class<?>> classes = new ArrayList<>();

    int startSubPath = Paths.get(cl.getResource("").toURI()).getNameCount();
    Files.walk(Paths.get(cl.getResource(packagePath).toURI()))
        .filter(Files::isRegularFile)
        .filter(path -> path.getFileName().toString().endsWith(".class"))
        .forEach(path -> {
          String className = path
              .subpath(startSubPath, path.getNameCount()).toString()
              .replaceAll("[/\\\\]", ".");
          className = className.substring(0, className.lastIndexOf('.'));
          try {
            Class<?> cls = Class.forName(className);
            if (isComponent(cls)) {
              classes.add(cls);
            }
          } catch (ClassNotFoundException e) {
            throw new ConfigurationException("Class not found during path scan.", e);
          }
        });

    return classes;
  }

  private void registerComponentClasses(final List<Class<?>> classes,
      final BeanFactory beanFactory) {
    List<Class> currentList = new ArrayList<>(classes);
    while (!currentList.isEmpty()) {
      int classCount = currentList.size();
      for (Iterator<Class> it = currentList.iterator(); it.hasNext(); ) {
        Class cls = it.next();
        if (canRegister(cls, beanFactory)) {
          beanFactory.registerBean(getComponentName(cls), createComponent(cls, beanFactory));
          it.remove();
        }
      }
      if (classCount == currentList.size()) {
        throw new ConfigurationException(
            "Unable to register all scanned components. Check for unresolved or cycled dependencies. "
                + "Unregistered component candidates: " + currentList.toString());
      }
    }
  }

  private boolean isComponent(Class cls) {
    return cls.isAnnotationPresent(Component.class)
        || cls.isAnnotationPresent(Service.class)
        || cls.isAnnotationPresent(Controller.class)
        || cls.isAnnotationPresent(Repository.class);
  }

  private boolean canRegister(Class cls, BeanFactory beanFactory) {
    Constructor[] ctors = cls.getConstructors();
    if (ctors.length > 1) {
      throw new ConfigurationException("Ambiguous constructor for class " + cls.getName());
    }
    Constructor ctor = ctors[0];
    if (ctor.getParameterCount() > 0 && !ctor.isAnnotationPresent(Autowiring.class)) {
      throw new ConfigurationException(
          "Can not use non-empty constructor without Autowiring for class " + cls.getName());
    }
    for (Parameter parameter : ctor.getParameters()) {
      if (!parameter.isAnnotationPresent(Qualifier.class)) {
        throw new ConfigurationException(
            "No qualifier found while autowiring for constructor parameter "
                + parameter.getType().getName() + " of class " + cls.getName());
      }
      final String parameterName = parameter.getAnnotation(Qualifier.class).value();
      if (beanFactory.getBean(parameterName, parameter.getType()) == null) {
        return false;
      }
    }

    return true;
  }


  private void injectFields(List<Class<?>> classes, BeanFactory beanFactory) {
    for (Class<?> cls : classes) {
      for (Field field : cls.getDeclaredFields()) {
        if (!field.isAnnotationPresent(Autowiring.class)) {
          continue;
        }
        if (!field.isAnnotationPresent(Qualifier.class)) {
          throw new ConfigurationException(
              "No qualifier found while autowiring for field " + field.getType().getName()
                  + " of class " + cls.getName());
        }
        Object bean = beanFactory.getBean(getComponentName(cls), cls);
        try {
          field.setAccessible(true);
          Object fieldValue = field.get(bean);
          if (fieldValue != null) {
            continue; //already set or injected
          }
          Object beanToInject = beanFactory
              .getBean(field.getAnnotation(Qualifier.class).value(), field.getType());
          field.set(bean, beanToInject);
        } catch (IllegalAccessException e) {
          throw new ConfigurationException(
              "Error while injecting field " + field.getType().getName()
                  + " for class " + cls.getName(), e);
        }
      }
    }
  }

  private void injectInterfaces(List<Class<?>> classes, BeanFactory beanFactory) {
    for (Class<?> cls : classes) {
      for (Method method : cls.getDeclaredMethods()) {
        if (!method.isAnnotationPresent(Autowiring.class)) {
          continue;
        }
        Object bean = beanFactory.getBean(getComponentName(cls), cls);
        Parameter[] parameters = method.getParameters();
        Object[] actualParameters = new Object[parameters.length];
        for (int i = 0; i < parameters.length; i++) {
          Parameter parameter = parameters[i];
          if (!parameter.isAnnotationPresent(Qualifier.class)) {
            throw new ConfigurationException(
                "No qualifier found while autowiring for method parameter "
                    + parameter.getType().getName() + " of method " + method.getName()
                    + " of class " + cls.getName());
          }
          actualParameters[i] = beanFactory
              .getBean(parameter.getAnnotation(Qualifier.class).value(), parameter.getType());
        }
        try {
          method.invoke(bean, actualParameters);
        } catch (IllegalAccessException | InvocationTargetException e) {
          throw new ConfigurationException(
              "Error while interface injection method invocation for method " + method.getName()
                  + " for class " + cls.getName(), e);
        }
      }
    }
  }

  public BeanFactory getBeanFactory() {
    return beanFactory;
  }
}
