package edu.ukma.cop.framework.core;

import edu.ukma.cop.framework.parser.Bean;
import edu.ukma.cop.framework.parser.ParserType;
import edu.ukma.cop.framework.parser.SaxParser;
import java.util.List;

public class XmlBeanDefinitionReader {

  private List<Bean> beanList;
  private List<Bean> interceptorList;
  private ParserType parserType;
  private boolean validating;

  public XmlBeanDefinitionReader() {
    parserType = ParserType.SAX;
    validating = false;
  }

  public List<Bean> getBeanList() {
    return beanList;
  }

  public List<Bean> getInterceptorList() {
    return interceptorList;
  }

  public boolean getValidating() {
    return validating;
  }

  public void setValidating(boolean validating) {
    this.validating = validating;
  }

  public void setParserType(ParserType parserType) {
    this.parserType = parserType;
  }

  public void loadBeanDefinitions(String fileName) {

    switch (parserType) {
      case SAX:
        beanList = new SaxParser(fileName).getBeanList();
        interceptorList = new SaxParser(fileName).getInterceptorList();
        break;
      default:
        throw new IllegalArgumentException();
    }
  }
}
