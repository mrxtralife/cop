package edu.ukma.cop.framework.core;

import static edu.ukma.cop.framework.util.BeanUtils.createComponent;

import edu.ukma.cop.framework.parser.Bean;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import org.apache.commons.lang3.tuple.Pair;

public class XmlBeanFactory implements BeanFactory {

  private HashMap<String, Pair<Object, BeanScope>> beanTable = new HashMap<>();
  private HashMap<String, Object> interceptorTable = new HashMap<>();

  XmlBeanFactory() {

  }

  XmlBeanFactory(String xmlFilePath, XmlBeanDefinitionReader xbdr) {
    xbdr.loadBeanDefinitions(xmlFilePath);
    generateBeans(xbdr.getBeanList());
    setupInterceptors(xbdr.getInterceptorList());
  }

  private void generateBeans(List<Bean> beanList) {
    for (Bean bean : beanList) {
      try {
        final Class<?> clazz = Class.forName(bean.getClassName());
        Constructor<?> ctor;
        Object object;

        List<String> ctorArgs = bean.getConstructorArg();

        if (!ctorArgs.isEmpty()) {

          Class<?>[] consClasses = new Class[ctorArgs.size() / 2];

          for (int i = 0, j = 0; i < ctorArgs.size(); i += 2) {
            if (ctorArgs.get(i) == null || ctorArgs.get(i).contentEquals("String")) {
              consClasses[j] = String.class;
            } else if (classLibrary.containsKey(ctorArgs.get(i))) {
              consClasses[j] = getPrimitiveClassForName(ctorArgs.get(i));
            } else {
              consClasses[j] = Class.forName(ctorArgs.get(i));
            }
            j++;
          }
          ctor = clazz.getConstructor(consClasses);
          Object[] consArgs = new Object[consClasses.length];
          for (int i = 1, j = 0; i < ctorArgs.size(); i += 2) {
            if (consClasses[j].isPrimitive()) {
              consArgs[j] =
                  getWrapperClassValueForPrimitiveType(consClasses[j], ctorArgs.get(i));
            } else {
              consArgs[j] = consClasses[j].cast(ctorArgs.get(i));
            }
            j++;
          }
          object = ctor.newInstance(consArgs);
        } else {
          ctor = clazz.getConstructor();
          object = ctor.newInstance();
        }

        List<String> props = bean.getProperties();

        if (!props.isEmpty()) {
          for (int i = 0; i < props.size(); i++) {
            char first = Character.toUpperCase(props.get(i).charAt(0));
            String methodName = "set" + first + props.get(i).substring(1);
            Method method = object.getClass().getMethod(methodName, props.get(i + 1).getClass());
            method.invoke(object, props.get(i + 1));
            i++;
          }
        }

        beanTable.put(bean.getName(), Pair.of(object, bean.getScope()));
      } catch (Exception ex) {
        ex.printStackTrace();
      }
    }
  }

  private void setupInterceptors(List<Bean> interceptorList) {
    for (Bean b : interceptorList) {
      try {
        final Class<?> clazz = Class.forName(b.getClassName());
        Object interceptor = clazz.getConstructor().newInstance();
        interceptorTable.put(b.getName(), interceptor);
      } catch (Exception ex) {
        ex.printStackTrace();
      }
    }
  }

  @Override
  public void registerBean(String name, Object bean) {
    registerBean(name, bean, BeanScope.SINGLETON);
  }

  @Override
  public void registerBean(String name, Object bean, BeanScope beanScope) {
    registerBean(name, Pair.of(bean, beanScope));
  }

  @Override
  public void registerBean(String name, Pair<Object, BeanScope> beanScopePair) {
    beanTable.put(name, beanScopePair);
  }

  @Override
  public Object getBean(String string) {
    Pair<Object, BeanScope> beanRecord = beanTable.get(string);
    if (BeanScope.SINGLETON == beanRecord.getRight()) {
      return beanRecord.getLeft();
    } else {
      return produceUponPrototype(beanRecord.getLeft());
    }
  }

  @SuppressWarnings("unchecked")
  @Override
  public <T> T getBean(String string, Class<T> type) {
    Pair<Object, BeanScope> beanRecord = beanTable.get(string);
    if (BeanScope.SINGLETON == beanRecord.getRight()) {
      return (T) beanRecord.getLeft();
    } else {
      return (T) produceUponPrototype(beanRecord.getLeft());
    }
  }

  @Override
  public Object[] getInterceptors() {
    return interceptorTable.values().toArray();
  }

  private Object produceUponPrototype(Object object) {
    Class<?> cls = object.getClass();

    return createComponent(cls, this).getLeft();
  }

}
