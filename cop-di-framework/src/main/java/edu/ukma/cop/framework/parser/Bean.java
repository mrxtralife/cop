package edu.ukma.cop.framework.parser;

import edu.ukma.cop.framework.core.BeanScope;
import java.util.ArrayList;
import java.util.List;

public class Bean {

  private String name;
  private String className;
  private List<String> constructorArg = new ArrayList<>();
  private List<String> properties = new ArrayList<>();
  private BeanScope scope;

  public List<String> getProperties() {
    return properties;
  }

  public void setProperties(List<String> properties) {
    this.properties = properties;
  }

  public List<String> getConstructorArg() {
    return constructorArg;
  }

  public void setConstructorArg(List<String> constructorArg) {
    this.constructorArg = constructorArg;
  }

  public BeanScope getScope() {
    return scope;
  }

  public void setScope(BeanScope scope) {
    this.scope = scope;
  }

  public String toString() {
    return name + " : " + className + constructorArg.toString() + ", " + properties.toString()
        + ", " + scope.toString();
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getClassName() {
    return className;
  }

  public void setClassName(String className) {
    this.className = className;
  }
}
