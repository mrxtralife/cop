package edu.ukma.cop.framework.parser;

import java.util.List;

public interface Parser {

  List<Bean> getBeanList();

  String toString();

}
