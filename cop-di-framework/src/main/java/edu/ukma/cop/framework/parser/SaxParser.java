package edu.ukma.cop.framework.parser;

import edu.ukma.cop.framework.core.BeanScope;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class SaxParser extends DefaultHandler implements Parser {

  private List<Bean> beanList = new ArrayList<>();
  private List<Bean> interceptorList = new ArrayList<>();
  private String xmlFileName;
  private Bean bean;

  public List<Bean> getBeanList() {
    return beanList;
  }

  public List<Bean> getInterceptorList() {
    return interceptorList;
  }

  public SaxParser(String xmlFileName) {
    this.xmlFileName = xmlFileName;
    parseDocument();
  }

  private void parseDocument() {
    SAXParserFactory factory = SAXParserFactory.newInstance();

    try {
      SAXParser parser = factory.newSAXParser();
      parser.parse(xmlFileName, this);
    } catch (ParserConfigurationException e) {
      System.out.println("ParserConfig error");
    } catch (SAXException e) {
      System.out.println("SAXException : xml not well formed");
    } catch (IOException e) {
      System.out.println("IO error");
    }
  }

  public String toString() {
    StringBuilder sb = new StringBuilder();
    for (Bean bean : beanList) {
      sb.append(bean.toString()).append("; ");
    }

    return sb.toString();
  }

  @Override
  public void startElement(String s, String s1, String elementName, Attributes attributes) {
    if (elementName.equalsIgnoreCase("bean") || elementName.equalsIgnoreCase("interceptor")) {
      this.bean = new Bean();
      this.bean.setName(attributes.getValue("id"));
      this.bean.setClassName(attributes.getValue("class"));
      BeanScope scope = BeanScope.fromString(attributes.getValue("scope"));
      if (scope == null) {
        scope = BeanScope.SINGLETON;
      }
      this.bean.setScope(scope);
    }

    if (elementName.equalsIgnoreCase("constructor-arg")) {
      this.bean.getConstructorArg().add(attributes.getValue("type"));
      this.bean.getConstructorArg().add(attributes.getValue("value"));
    }

    if (elementName.equalsIgnoreCase("property")) {
      this.bean.getProperties().add(attributes.getValue("name"));
      this.bean.getProperties().add(attributes.getValue("value"));
    }
  }

  @Override
  public void endElement(String s, String s1, String element) {
    if (element.equals("bean")) {
      beanList.add(bean);
    }

    if (element.equals("interceptor")) {
      interceptorList.add(this.bean);
    }
  }
}
