package edu.ukma.cop.framework.util;

import edu.ukma.cop.framework.annotation.Component;
import edu.ukma.cop.framework.annotation.Controller;
import edu.ukma.cop.framework.annotation.Qualifier;
import edu.ukma.cop.framework.annotation.Repository;
import edu.ukma.cop.framework.annotation.Scope;
import edu.ukma.cop.framework.annotation.Service;
import edu.ukma.cop.framework.core.BeanFactory;
import edu.ukma.cop.framework.core.BeanScope;
import edu.ukma.cop.framework.exception.ConfigurationException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Parameter;
import org.apache.commons.lang3.tuple.Pair;

public class BeanUtils {

  private BeanUtils() {
    //Util class
  }

  public static Pair<Object, BeanScope> createComponent(Class cls, BeanFactory beanFactory) {
    Constructor[] ctors = cls.getConstructors();
    Constructor ctor = ctors[0];
    Parameter[] parameters = ctor.getParameters();
    Object[] actualParameters = new Object[parameters.length];
    for (int i = 0; i < parameters.length; i++) {
      actualParameters[i] = beanFactory
          .getBean(parameters[i].getAnnotation(Qualifier.class).value(), parameters[i].getType());
    }
    try {
      Object obj = ctor.newInstance(actualParameters);
      BeanScope scope = null;
      if (cls.isAnnotationPresent(Scope.class)) {
        scope = BeanScope.fromString(getAnnotationValue(cls, Scope.class));
      }
      if (scope == null) {
        scope = BeanScope.SINGLETON;
      }
      return Pair.of(obj, scope);
    } catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
      throw new ConfigurationException(
          "Error while creating component bean for class " + cls.getName(), e);
    }
  }

  public static String getComponentName(Class cls) {
    String componentName = "";
    if (cls.isAnnotationPresent(Component.class)) {
      componentName = getAnnotationValue(cls, Component.class);
    }
    if (cls.isAnnotationPresent(Service.class)) {
      componentName = getAnnotationValue(cls, Service.class);
    }
    if (cls.isAnnotationPresent(Controller.class)) {
      componentName = getAnnotationValue(cls, Controller.class);
    }
    if (cls.isAnnotationPresent(Repository.class)) {
      componentName = getAnnotationValue(cls, Repository.class);
    }
    if (componentName.isEmpty()) {
      final String className = cls.getSimpleName();
      componentName = Character.toLowerCase(className.charAt(0)) + className.substring(1);
    }
    return componentName;
  }

  public static String getAnnotationValue(Class<?> cls, Class<? extends Annotation> annotationClass) {
    Annotation annotation = cls.getAnnotation(annotationClass);
    try {
      return annotation.annotationType().getMethod("value").invoke(annotation).toString();
    } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
      throw new ConfigurationException(
          "Error while getting component qualifier name for class " + cls.getName(), e);
    }
  }


}
