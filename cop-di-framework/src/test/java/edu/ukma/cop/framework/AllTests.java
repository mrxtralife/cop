package edu.ukma.cop.framework;

import edu.ukma.cop.framework.core.GenericXmlAppContextTest;
import edu.ukma.cop.framework.parser.SaxParserTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ SaxParserTest.class, GenericXmlAppContextTest.class })
public class AllTests {

}