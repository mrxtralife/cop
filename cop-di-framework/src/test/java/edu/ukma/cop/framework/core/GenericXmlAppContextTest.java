package edu.ukma.cop.framework.core;

import static org.junit.Assert.assertNotNull;

import org.junit.Ignore;
import org.junit.Test;
import org.mockito.Mockito;


public class GenericXmlAppContextTest {

  public <T> T getMockObject(Class<T> classToBeMocked) throws Exception {
    return Mockito.mock(classToBeMocked);
  }

  @Test
  @Ignore
  public void testCreationDefaultConstructor() {
    GenericXmlApplicationContext tester = new GenericXmlApplicationContext();
    assertNotNull(tester);
  }

  @Test
  @Ignore
  public void testCreationStringConstructor() {
    GenericXmlApplicationContext tester = new GenericXmlApplicationContext("filename");
    assertNotNull(tester);
  }

  @Test
  @Ignore
  public void testGetBeanFactory() {
    GenericXmlApplicationContext tester = new GenericXmlApplicationContext();
    assertNotNull(tester.getBeanFactory());
  }

  @Test
  @Ignore
  public void testGetBean() {
    GenericXmlApplicationContext tester = new GenericXmlApplicationContext();
    assertNotNull(tester.getBeanFactory().getBean("bus"));
  }

//    @Test
//    public void testGetBeanGeneric() {
//        GenericXmlApplicationContext tester = new GenericXmlApplicationContext();
//        assertNotNull((Bus)tester.getBeanFactory().getBean("bus", Bus.class));
//    }

  @Test
  @Ignore
  public void testSetParserTypeSupported() {
    GenericXmlApplicationContext tester = new GenericXmlApplicationContext();
    assertNotNull(tester);
  }
}
