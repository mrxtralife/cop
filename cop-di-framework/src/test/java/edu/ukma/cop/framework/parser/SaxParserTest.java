package edu.ukma.cop.framework.parser;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;

public class SaxParserTest {

  private String xmlFilePath = getClass().getResource("/GS_SpringXMLConfig.xml").getPath();

  @Test
  public void testCreation() {
    SaxParser tester = new SaxParser(xmlFilePath);
    assertNotNull(tester);
  }

  @Test
  public void testGetBeanList() {
    SaxParser tester = new SaxParser(xmlFilePath);
    assertNotNull(tester.getBeanList());
  }

  @Test
  public void testToString() {
    SaxParser tester = new SaxParser(xmlFilePath);
    assertNotNull(tester.toString());
  }
}
