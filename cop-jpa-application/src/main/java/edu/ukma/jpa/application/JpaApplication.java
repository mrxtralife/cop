package edu.ukma.jpa.application;

import edu.ukma.cop.jpa.framework.core.DatabaseSession;
import edu.ukma.jpa.application.entity.Address;
import edu.ukma.jpa.application.entity.Author;
import edu.ukma.jpa.application.entity.Book;
import edu.ukma.jpa.application.entity.Genre;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class JpaApplication {

  public static void main(String[] args) {
    DatabaseSession session = new DatabaseSession();

    session.open();

    // Test load by id
    Author author = session.load(2, Author.class);
    System.out.println("Author with id 2: " + author);

    //Test create composite entity
    Book book = new Book();
    book.setName("Ender Game 4");
    book.setIsbn("1133-23-33-555" + ThreadLocalRandom.current().nextInt(999, 9999));
    book.setAuthorId(3);
    Genre genreSciFi = session.loadByFieldValue("name", "Science fiction", Genre.class);
    book.setGenre(genreSciFi);

    session.save(book);
    System.out.println("Book: " + book);

    //Test load by id
    Book book2 = session.load(1, Book.class);
    System.out.println("Book 2: " + book2);

    //Test load all
    List<Author> authors = session.loadAll(Author.class);
    System.out.println("All authors: " + authors);

    //Test create
    Author newAuthor = new Author();
    newAuthor.setFirstName("Leo");
    newAuthor.setLastName("Tolstoy");
    newAuthor.setYears(130);

    newAuthor = session.save(newAuthor);
    System.out.println("New author: " + newAuthor);

    //Test cascade create/update
    author.setFirstName(author.getFirstName() + " updated");
    author.setLastName(author.getLastName() + " updated");
    author.getAddress().setCity("New York");
    author.getAddress().setCountry("Usa");
    Author updatedAuthor = session.save(author);
    System.out.println("Updated author:" + updatedAuthor);

    //Test delete
    Genre genreForDelete = new Genre();
    genreForDelete.setName("Test name to delete");
    session.save(genreForDelete);
    System.out.println("Genre created: " + genreForDelete);

    session.delete(genreForDelete);
    System.out.println("Genre deleted.");

    //Test cascade delete
    Address cAddress = new Address();
    cAddress.setCountry("Ukraine");
    cAddress.setCity("Kyiv");

    Author cAuthor = new Author();
    cAuthor.setAddress(cAddress);
    cAuthor.setFirstName("Vasya");
    cAuthor.setLastName("Pupkin");
    cAuthor.setYears(18);
    cAuthor.setBirthday(new Date());

    List<Book> cBooks = new ArrayList<>();
    Book cBook1 = new Book();
    cBook1.setGenre(genreSciFi);
    cBook1.setIsbn("111-22-33-4456-7" + ThreadLocalRandom.current().nextInt(999, 9999));
    cBook1.setName("Test Book 1");
    cBooks.add(cBook1);

    Book cBook2 = new Book();
    cBook2.setGenre(genreSciFi);
    cBook2.setIsbn("222-33-44-46-8" + ThreadLocalRandom.current().nextInt(999, 9999));
    cBook2.setName("Test Book 2");
    cBooks.add(cBook2);

    cAuthor.setBooks(cBooks);

    session.save(cAuthor);

    session.delete(cAuthor);

    //Test native query
    List<Author> nativeAuthors = session.loadAllByNativeQuery(Author.class);
    System.out.println("List by native query: " + nativeAuthors);

    session.close();
  }

}
