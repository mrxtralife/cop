package edu.ukma.jpa.application.entity;

import edu.ukma.cop.jpa.framework.annotation.Column;
import edu.ukma.cop.jpa.framework.annotation.Entity;
import edu.ukma.cop.jpa.framework.annotation.Id;
import edu.ukma.cop.jpa.framework.annotation.Table;

@Entity
@Table("address")
public class Address {

  @Id
  @Column("id")
  private long id;

  @Column
  private String country;

  @Column
  private String city;

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getCountry() {
    return country;
  }

  public void setCountry(String country) {
    this.country = country;
  }

  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }

  @Override
  public String toString() {
    return "Address{" +
        "id=" + id +
        ", country='" + country + '\'' +
        ", city='" + city + '\'' +
        '}';
  }
}
