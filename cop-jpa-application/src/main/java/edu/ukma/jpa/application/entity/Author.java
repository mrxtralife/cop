package edu.ukma.jpa.application.entity;

import edu.ukma.cop.jpa.framework.annotation.Column;
import edu.ukma.cop.jpa.framework.annotation.Entity;
import edu.ukma.cop.jpa.framework.annotation.Id;
import edu.ukma.cop.jpa.framework.annotation.OnDelete;
import edu.ukma.cop.jpa.framework.annotation.OneToMany;
import edu.ukma.cop.jpa.framework.annotation.OneToOne;
import edu.ukma.cop.jpa.framework.annotation.Query;
import edu.ukma.cop.jpa.framework.annotation.Table;
import java.util.Date;
import java.util.List;

@Entity
@Table("author")
@Query("select id, first_name, last_name, birthday, years, address_id from author where address_id is not null;")
public class Author {

  @Id
  @Column("id")
  private long id;

  @Column("first_name")
  private String firstName;

  @Column("last_name")
  private String lastName;

  @Column
  private Date birthday;

  @Column
  private int years;

  @OneToOne(joinColumn = "address_id", onDelete = OnDelete.CASCADE)
  private Address address;

  @OneToMany(invertJoinColumn = "author_id", onDelete = OnDelete.CASCADE)
  private List<Book> books;

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public Date getBirthday() {
    return birthday;
  }

  public void setBirthday(Date birthday) {
    this.birthday = birthday;
  }

  public int getYears() {
    return years;
  }

  public void setYears(int years) {
    this.years = years;
  }

  public Address getAddress() {
    return address;
  }

  public void setAddress(Address address) {
    this.address = address;
  }

  public List<Book> getBooks() {
    return books;
  }

  public void setBooks(List<Book> books) {
    this.books = books;
  }

  @Override
  public String toString() {
    return "Author{" +
        "id=" + id +
        ", firstName='" + firstName + '\'' +
        ", lastName='" + lastName + '\'' +
        ", birthday=" + birthday +
        ", years=" + years +
        ", address=" + address +
        ", books=" + books +
        '}';
  }
}
