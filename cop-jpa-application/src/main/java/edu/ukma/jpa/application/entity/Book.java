package edu.ukma.jpa.application.entity;

import edu.ukma.cop.jpa.framework.annotation.Column;
import edu.ukma.cop.jpa.framework.annotation.Entity;
import edu.ukma.cop.jpa.framework.annotation.Id;
import edu.ukma.cop.jpa.framework.annotation.Loading;
import edu.ukma.cop.jpa.framework.annotation.ManyToOne;
import edu.ukma.cop.jpa.framework.annotation.Table;

@Entity
@Table("book")
public class Book {

  @Id
  @Column
  private long id;

  @Column
  private String name;

  @Column
  private String isbn;

  @ManyToOne(joinColumn = "genre_id", loading = Loading.LAZY)
  private Genre genre;

  @Column("author_id")
  private long authorId;

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getIsbn() {
    return isbn;
  }

  public void setIsbn(String isbn) {
    this.isbn = isbn;
  }

  public Genre getGenre() {
    return genre;
  }

  public void setGenre(Genre genre) {
    this.genre = genre;
  }

  public long getAuthorId() {
    return authorId;
  }

  public void setAuthorId(long authorId) {
    this.authorId = authorId;
  }

  @Override
  public String toString() {
    return "Book{" +
        "id=" + id +
        ", name='" + name + '\'' +
        ", isbn='" + isbn + '\'' +
        ", genre=" + genre +
        ", authorId=" + authorId +
        '}';
  }
}
