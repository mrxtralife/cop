package edu.ukma.jpa.application.entity;

import edu.ukma.cop.jpa.framework.annotation.Column;
import edu.ukma.cop.jpa.framework.annotation.Entity;
import edu.ukma.cop.jpa.framework.annotation.Id;
import edu.ukma.cop.jpa.framework.annotation.Table;

@Entity
@Table("genre")
public class Genre {

  @Id
  @Column
  private long id;

  @Column
  private String name;

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @Override
  public String toString() {
    return "Genre{" +
        "id=" + id +
        ", name='" + name + '\'' +
        '}';
  }
}
