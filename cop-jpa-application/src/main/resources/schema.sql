CREATE TABLE `genre`
(
  `id`   int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `address`
(
  `id`      int(11) NOT NULL AUTO_INCREMENT,
  `country` varchar(255) DEFAULT NULL,
  `city`    varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
);


CREATE TABLE `author`
(
  `id`         int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name`  varchar(255) DEFAULT NULL,
  `birthday`   date         DEFAULT NULL,
  `years`      int(11) DEFAULT NULL,
  `address_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `address_id_UNIQUE` (`address_id`),
  KEY          `address_fk_idx` (`address_id`),
  CONSTRAINT `address_fk` FOREIGN KEY (`address_id`) REFERENCES `address` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
);

CREATE TABLE `book`
(
  `id`        int(11) NOT NULL AUTO_INCREMENT,
  `name`      varchar(255) NOT NULL,
  `isbn`      varchar(20)  NOT NULL,
  `author_id` int(11) DEFAULT NULL,
  `genre_id`  int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `isbn_UNIQUE` (`isbn`),
  KEY         `author_id_fk_idx` (`author_id`),
  KEY         `genre_id_fk_idx` (`genre_id`),
  CONSTRAINT `author_id_fk` FOREIGN KEY (`author_id`) REFERENCES `author` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `genre_id_fk` FOREIGN KEY (`genre_id`) REFERENCES `genre` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
);
