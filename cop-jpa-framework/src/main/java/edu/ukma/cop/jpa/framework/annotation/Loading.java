package edu.ukma.cop.jpa.framework.annotation;

public enum Loading {
  EAGER, LAZY
}
