package edu.ukma.cop.jpa.framework.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
public @interface ManyToOne {

  String joinColumn() default "";

  Loading loading() default Loading.EAGER;

}
