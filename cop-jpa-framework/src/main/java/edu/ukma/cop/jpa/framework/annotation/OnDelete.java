package edu.ukma.cop.jpa.framework.annotation;

public enum OnDelete {
  CASCADE, SET_NULL, DO_NOTHING
}
