package edu.ukma.cop.jpa.framework.core;

import static edu.ukma.cop.jpa.framework.util.ClassUtils.convertToEntity;
import static edu.ukma.cop.jpa.framework.util.ClassUtils.foreignKeyField;
import static edu.ukma.cop.jpa.framework.util.ClassUtils.getForeignKeyColumn;
import static edu.ukma.cop.jpa.framework.util.ClassUtils.getMemberCollectionsList;
import static edu.ukma.cop.jpa.framework.util.ClassUtils.getMemberEntitiesList;
import static edu.ukma.cop.jpa.framework.util.ClassUtils.getNativeQuery;
import static edu.ukma.cop.jpa.framework.util.ClassUtils.idValue;
import static edu.ukma.cop.jpa.framework.util.ClassUtils.isCascadeDelete;
import static edu.ukma.cop.jpa.framework.util.ClassUtils.isLazyLoading;
import static edu.ukma.cop.jpa.framework.util.ClassUtils.isNewEntity;
import static edu.ukma.cop.jpa.framework.util.ClassUtils.setEntityId;
import static edu.ukma.cop.jpa.framework.util.SqlUtils.createDeleteById;
import static edu.ukma.cop.jpa.framework.util.SqlUtils.createInsert;
import static edu.ukma.cop.jpa.framework.util.SqlUtils.createSelectAll;
import static edu.ukma.cop.jpa.framework.util.SqlUtils.createSelectByFieldValue;
import static edu.ukma.cop.jpa.framework.util.SqlUtils.createSelectByIdQuery;
import static edu.ukma.cop.jpa.framework.util.SqlUtils.createUpdate;

import edu.ukma.cop.jpa.framework.exception.PersistenceException;
import java.io.IOException;
import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Triple;

public class DatabaseSession {

  private final static String DEFAULT_PROPERTIES_FILE = "database.properties";
  private final static String DATABASE_URL = "database.url";
  private final static String DATABASE_DRIVER = "database.driver";
  private final static String DATABASE_USERNAME = "database.username";
  private final static String DATABASE_PASSWORD = "database.password";

  private final Properties props = new Properties();
  private Connection connection;

  public DatabaseSession() {
    this(DEFAULT_PROPERTIES_FILE);
  }

  public DatabaseSession(String propertiesFilename) {
    try {
      this.props.load(getClass().getResourceAsStream("/" + propertiesFilename));
    } catch (IOException e) {
      throw new PersistenceException("Error while loading database connection properties.", e);
    }
    try {
      Class.forName(getProperty(DATABASE_DRIVER));
    } catch (ClassNotFoundException e) {
      throw new PersistenceException("Error while loading database driver.", e);
    }
  }

  public void open() {
    try {
      connection = DriverManager.getConnection(
          getProperty(DATABASE_URL),
          getProperty(DATABASE_USERNAME),
          getProperty(DATABASE_PASSWORD));

    } catch (SQLException e) {
      throw new PersistenceException("Error while opening connection", e);
    }
  }

  public void close() {
    if (connection != null) {
      try {
        connection.close();
      } catch (SQLException e) {
        throw new PersistenceException("Error while closing connection.", e);
      }
    }
  }

  private String getProperty(String propertyName) {
    return this.props.getProperty(propertyName);
  }

  public <T> T load(long id, Class<T> tClass) {
    requireConnection();

    String sql = createSelectByIdQuery(id, tClass);
    try (Statement st = connection.createStatement(); ResultSet resultSet = st.executeQuery(sql)) {
      if (resultSet.next()) {
        T entity = convertToEntity(resultSet, tClass);
        loadMemberEntities(entity, resultSet);
        loadMemberCollections(entity);
        return entity;
      } else {
        return null;
      }
    } catch (SQLException e) {
      throw new PersistenceException("Error while loading entity " + tClass.getName(), e);
    }
  }

  public <T> List<T> loadAll(Class<T> tClass) {
    requireConnection();

    String sql = createSelectAll(tClass);
    return loadListByRequest(sql, tClass);
  }

  public <T> List<T> loadAllByNativeQuery(Class<T> tClass) {
    requireConnection();

    String sql = getNativeQuery(tClass);
    if (StringUtils.isEmpty(sql)) {
      sql = createSelectAll(tClass);
    }
    return loadListByRequest(sql, tClass);
  }

  public <T> List<T> loadAllByFieldValue(String fieldName, Object value, Class<T> tClass) {
    requireConnection();

    String sql = createSelectByFieldValue(fieldName, value, tClass);
    return loadListByRequest(sql, tClass);
  }

  public <T> T loadByFieldValue(String fieldName, Object value, Class<T> tClass) {
    requireConnection();

    List<T> resultList = loadAllByFieldValue(fieldName, value, tClass);
    if (resultList.isEmpty()) {
      return null;
    }
    return resultList.get(0);
  }

  private <T> List<T> loadListByRequest(String sql, Class<T> tClass) {
    try (Statement st = connection.createStatement(); ResultSet resultSet = st.executeQuery(sql)) {
      List<T> resultList = new ArrayList<>();
      while (resultSet.next()) {
        T entity = convertToEntity(resultSet, tClass);
        loadMemberEntities(entity, resultSet);
        loadMemberCollections(entity);
        resultList.add(entity);
      }
      return resultList;
    } catch (SQLException e) {
      throw new PersistenceException("Error while loading entity list", e);
    }
  }

  private <T> void loadMemberEntities(T entity, ResultSet resultSet) throws SQLException {
    Class<?> cls = entity.getClass();

    List<Triple<String, String, String>> memberEntitiesInfo = getMemberEntitiesList(cls);
    for (Triple<String, String, String> memberEntityInfo : memberEntitiesInfo) {
      try {
        Field field = cls.getDeclaredField(memberEntityInfo.getMiddle());
        if (isLazyLoading(field)) {
          continue;
        }
        field.setAccessible(true);
        Class<?> memberEntityClass = Class.forName(memberEntityInfo.getRight());
        long entityId = resultSet.getLong(memberEntityInfo.getLeft());
        Object memberEntity = load(entityId, memberEntityClass);
        field.set(entity, memberEntity);
      } catch (ClassNotFoundException | NoSuchFieldException | IllegalAccessException e) {
        throw new PersistenceException(
            "Error while creating member entity " + memberEntityInfo.getRight()
                + " for entity " + cls.getName(), e);
      }
    }
  }

  private <T> void loadMemberCollections(T entity) {
    Class<?> cls = entity.getClass();

    List<Triple<String, String, String>> memberCollectionsInfo = getMemberCollectionsList(cls);
    for (Triple<String, String, String> memberCollectionInfo : memberCollectionsInfo) {
      try {
        Field field = cls.getDeclaredField(memberCollectionInfo.getLeft());
        if (isLazyLoading(field)) {
          continue;
        }
        final String foreignKeyColumn = getForeignKeyColumn(field);
        final long entityId = idValue(entity);
        Class<?> contentType = Class.forName(memberCollectionInfo.getRight());
        Object list = loadAllByFieldValue(foreignKeyColumn, entityId, contentType);
        field.setAccessible(true);
        field.set(entity, list);
      } catch (NoSuchFieldException | ClassNotFoundException | IllegalAccessException e) {
        throw new PersistenceException(
            "Error while loading member collection " + memberCollectionInfo.getLeft()
                + " for entity " + cls.getName(), e);
      }
    }
  }

  public <T> T save(T entity) {
    requireConnection();

    saveMemberEntities(entity);

    String sql = isNewEntity(entity) ? createInsert(entity) : createUpdate(entity);
    try (Statement statement = connection.createStatement()) {
      int queryResult = statement.executeUpdate(sql, Statement.RETURN_GENERATED_KEYS);
      if (queryResult == 0) {
        throw new PersistenceException("No rows were affected during query execution.");
      }
      try (ResultSet generatedKey = statement.getGeneratedKeys()) {
        if (generatedKey.next()) {
          setEntityId(entity, generatedKey.getLong(1));
        }
      }
      saveMemberCollections(entity);
      return entity;
    } catch (SQLException e) {
      throw new PersistenceException("Error while saving entity " + entity, e);
    }
  }

  public <T> void delete(long id, Class<T> tClass) {
    deleteMemberEntities(id, tClass);
    deleteMemberCollections(id, tClass);

    String sql = createDeleteById(id, tClass);
    try (Statement st = connection.createStatement()) {
      st.executeUpdate(sql);
    } catch (SQLException e) {
      throw new PersistenceException("Error while deleting entity " + tClass.getName(), e);
    }
  }

  public <T> void delete(T entity) {
    delete(idValue(entity), entity.getClass());
  }

  private <T> void saveMemberEntities(T entity) {
    Class<?> cls = entity.getClass();

    List<Triple<String, String, String>> memberEntitiesInfo = getMemberEntitiesList(cls);
    for (Triple<String, String, String> memberEntityInfo : memberEntitiesInfo) {
      try {
        Field field = cls.getDeclaredField(memberEntityInfo.getMiddle());
        field.setAccessible(true);
        Object memberEntity = field.get(entity);
        if (memberEntity == null) {
          continue;
        }
        Object savedMemberEntity = save(memberEntity);
        field.set(entity, savedMemberEntity);
      } catch (NoSuchFieldException | IllegalAccessException e) {
        throw new PersistenceException(
            "Error while saving member entity " + memberEntityInfo.getRight()
                + " for entity " + cls.getName(), e);
      }
    }
  }

  private <T> void saveMemberCollections(T entity) {
    Class<?> cls = entity.getClass();

    List<Triple<String, String, String>> memberCollectionsInfo = getMemberCollectionsList(cls);
    for (Triple<String, String, String> memberCollectionInfo : memberCollectionsInfo) {
      try {
        Field field = cls.getDeclaredField(memberCollectionInfo.getLeft());
        field.setAccessible(true);
        List memberCollection = (List) field.get(entity);
        if (memberCollection == null) {
          continue;
        }
        String foreignKeyColumnName = getForeignKeyColumn(field);
        long entityId = idValue(entity);
        for (Object member : memberCollection) {
          Class<?> memberCls = member.getClass();
          Field foreignKeyField = foreignKeyField(memberCls, foreignKeyColumnName);
          foreignKeyField.setAccessible(true);
          foreignKeyField.set(member, entityId);
          save(member);
        }
      } catch (NoSuchFieldException | IllegalAccessException e) {
        throw new PersistenceException(
            "Error while saving member collections for entity " + cls.getName(), e);
      }
    }
  }

  private <T> void deleteMemberEntities(long id, Class<T> tClass) {
    List<Triple<String, String, String>> membersEntitiesInfo = getMemberEntitiesList(tClass);
    for (Triple<String, String, String> memberEntityInfo : membersEntitiesInfo) {
      try {
        Field field = tClass.getDeclaredField(memberEntityInfo.getMiddle());
        if (isCascadeDelete(field)) {
          T entity = load(id, tClass);
          field.setAccessible(true);
          Object memberEntity = field.get(entity);
          if (memberEntity == null) {
            continue;
          }
          delete(idValue(memberEntity), memberEntity.getClass());
        }
      } catch (NoSuchFieldException | IllegalAccessException e) {
        throw new PersistenceException(
            "Error while deleting member entity " + memberEntityInfo.getRight()
                + " for entity " + tClass.getName(), e);
      }
    }
  }

  private <T> void deleteMemberCollections(long id, Class<T> tClass) {
    List<Triple<String, String, String>> memberCollectionsInfo = getMemberCollectionsList(tClass);
    for (Triple<String, String, String> memberCollectionInfo : memberCollectionsInfo) {
      try {
        Field field = tClass.getDeclaredField(memberCollectionInfo.getLeft());
        if (isCascadeDelete(field)) {
          T entity = load(id, tClass);
          field.setAccessible(true);
          List memberCollection = (List) field.get(entity);
          if (memberCollection == null) {
            continue;
          }
          for (Object collectionItem : memberCollection) {
            delete(collectionItem);
          }
        }
      } catch (NoSuchFieldException | IllegalAccessException e) {
        throw new PersistenceException(
            "Error while deleting member collection for entity " + tClass.getName(), e);
      }
    }
  }

  private void requireConnection() {
    if (connection == null) {
      throw new PersistenceException("Connection is null. Try to open it before calling database");
    }
  }
}
