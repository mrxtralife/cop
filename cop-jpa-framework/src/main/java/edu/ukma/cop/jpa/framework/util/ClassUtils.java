package edu.ukma.cop.jpa.framework.util;

import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.commons.lang3.StringUtils.lastIndexOf;

import edu.ukma.cop.jpa.framework.annotation.Column;
import edu.ukma.cop.jpa.framework.annotation.Entity;
import edu.ukma.cop.jpa.framework.annotation.Id;
import edu.ukma.cop.jpa.framework.annotation.Loading;
import edu.ukma.cop.jpa.framework.annotation.ManyToOne;
import edu.ukma.cop.jpa.framework.annotation.OnDelete;
import edu.ukma.cop.jpa.framework.annotation.OneToMany;
import edu.ukma.cop.jpa.framework.annotation.OneToOne;
import edu.ukma.cop.jpa.framework.annotation.Query;
import edu.ukma.cop.jpa.framework.annotation.Table;
import edu.ukma.cop.jpa.framework.exception.PersistenceException;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.commons.lang3.tuple.Triple;

public class ClassUtils {

  private final static String STRING_CLASS = "java.lang.String";
  private final static String INTEGER_CLASS = "java.lang.Integer";
  private final static String INTEGER_PRIM = "int";
  private final static String LONG_CLASS = "java.lang.Long";
  private final static String LONG_PRIM = "long";
  private final static String DATE_CLASS = "java.util.Date";

  private ClassUtils() {
    //Util class
  }

  private static List<String> columnList(Class<?> cls, boolean includeIdColumn) {
    requireEntityType(cls);

    return Arrays.stream(cls.getDeclaredFields())
        .filter(field -> isColumn(field) || isMemberRelationship(field))
        .filter(field -> includeIdColumn || !isIdField(field))
        .map(ClassUtils::columnName)
        .collect(Collectors.toList());
  }

  public static String columnListString(Class<?> cls, boolean includeIdColumn) {
    requireEntityType(cls);

    List<String> columnList = columnList(cls, includeIdColumn);

    StringBuilder sb = new StringBuilder();
    for (String columnName : columnList) {
      sb.append(columnName).append(", ");
    }
    final String columnListString = sb.toString();
    return columnListString.substring(0, lastIndexOf(columnListString, ","));
  }

  private static String columnName(Field field) {
    String columnName = null;
    int found = 0;
    if (field.isAnnotationPresent(Column.class)) {
      columnName = field.getAnnotation(Column.class).value();
      found++;
    }
    if (field.isAnnotationPresent(OneToOne.class)) {
      columnName = field.getAnnotation(OneToOne.class).joinColumn();
      found++;
    }
    if (field.isAnnotationPresent(OneToMany.class)) {
      found++;
    }
    if (field.isAnnotationPresent(ManyToOne.class)) {
      columnName = field.getAnnotation(ManyToOne.class).joinColumn();
      found++;
    }
    if (found > 1) {
      throw new PersistenceException(
          "More than one persistence annotation found on field " + field.getName());
    }
    if (isBlank(columnName)) {
      columnName = field.getName();
    }
    return columnName;
  }

  public static String tableName(Class<?> cls) {
    requireEntityType(cls);

    String tableName = null;
    if (cls.isAnnotationPresent(Table.class)) {
      tableName = cls.getAnnotation(Table.class).value();
    }
    if (isBlank(tableName)) {
      tableName = cls.getSimpleName();
    }

    return tableName.toUpperCase();
  }

  public static String idColumn(Class<?> cls) {
    requireEntityType(cls);

    Field[] fields = cls.getDeclaredFields();
    for (Field field : fields) {
      if (field.isAnnotationPresent(Id.class)) {
        String idName = columnName(field);

        return idName.toUpperCase();
      }
    }
    throw new PersistenceException("No Id column found for Entity " + cls.getName());
  }

  public static <T> T convertToEntity(ResultSet resultSet, Class<T> cls) {
    try {
      T entity = cls.newInstance();
      return convertToEntity(resultSet, entity);
    } catch (InstantiationException | IllegalAccessException e) {
      throw new PersistenceException(
          "Error while instantiating new entity for class " + cls.getName(), e);
    }
  }

  public static <T> T convertToEntity(ResultSet resultSet, T entity) {
    Class<?> cls = entity.getClass();
    try {
      List<Triple<String, String, String>> fields = getFieldsWithTypes(cls);
      for (Triple<String, String, String> fieldTriple : fields) {
        Field field;
        switch (fieldTriple.getRight()) {
          case STRING_CLASS:
            String strValue = resultSet.getString(fieldTriple.getLeft());
            field = cls.getDeclaredField(fieldTriple.getMiddle());
            field.setAccessible(true);
            field.set(entity, strValue);
            break;
          case INTEGER_CLASS:
            int intValue = resultSet.getInt(fieldTriple.getLeft());
            field = cls.getDeclaredField(fieldTriple.getMiddle());
            field.setAccessible(true);
            field.set(entity, intValue);
            break;
          case DATE_CLASS:
            Date dateValue = resultSet.getDate(fieldTriple.getLeft());
            field = cls.getDeclaredField(fieldTriple.getMiddle());
            field.setAccessible(true);
            field.set(entity, dateValue);
            break;
          case LONG_CLASS:
            long longValue = resultSet.getLong(fieldTriple.getLeft());
            field = cls.getDeclaredField(fieldTriple.getMiddle());
            field.setAccessible(true);
            field.set(entity, longValue);
            break;
          default:
            break;
        }
      }
      return entity;
    } catch (IllegalAccessException | NoSuchFieldException | SQLException e) {
      throw new PersistenceException(
          "Error while creating resulting Entity for class " + cls.getName(), e);
    }
  }

  private static void requireEntityType(Class<?> cls) {
    if (!cls.isAnnotationPresent(Entity.class)) {
      throw new PersistenceException("Class " + cls.getName() + " is not Entity class.");
    }
  }

  private static boolean isPrimitiveType(String type) {
    return Arrays
        .asList(STRING_CLASS, INTEGER_CLASS, INTEGER_PRIM, LONG_CLASS, LONG_PRIM, DATE_CLASS)
        .contains(type);
  }

  /**
   * Creates list of persistent columns with field information for entity
   *
   * @param cls class to parse for resulting list
   * @return triple of Column name, Field name, Field type
   */
  private static List<Triple<String, String, String>> getFieldsWithTypes(Class<?> cls) {
    return Arrays.stream(cls.getDeclaredFields())
        .filter(field -> isColumn(field) || isMemberRelationship(field))
        .map(field -> Triple.of(columnName(field), field.getName(), fieldType(field)))
        .collect(Collectors.toList());
  }

  public static List<Triple<String, String, String>> getMemberEntitiesList(Class<?> cls) {
    return getFieldsWithTypes(cls).stream()
        .filter(triple -> !isPrimitiveType(triple.getRight()))
        .collect(Collectors.toList());
  }

  /**
   * @param cls Entity class
   * @return Triple of Field name, Collection type, Collection content type
   */
  public static List<Triple<String, String, String>> getMemberCollectionsList(Class<?> cls) {
    return Arrays.stream(cls.getDeclaredFields())
        .filter(ClassUtils::isCollectionRelationship)
        .peek(field -> {
          if (!"java.util.List".equals(fieldType(field))) {
            throw new PersistenceException(
                "OneToMany relationship require field of type List."
                    + " Found " + fieldType(field) + " for entity " + cls.getName());
          }
        })
        .map(field -> Triple.of(field.getName(), fieldType(field), genericFieldContentType(field)))
        .collect(Collectors.toList());
  }

  public static String getForeignKeyColumn(Field field) {
    if (!isCollectionRelationship(field)) {
      throw new PersistenceException("Wrong OneToMany field " + field.getType());
    }
    return field.getAnnotation(OneToMany.class).invertJoinColumn();
  }

  private static String fieldType(Field field) {
    final String typeName = field.getType().getName();
    switch (typeName) {
      case STRING_CLASS:
        return STRING_CLASS;
      case INTEGER_CLASS:
      case INTEGER_PRIM:
        return INTEGER_CLASS;
      case LONG_CLASS:
      case LONG_PRIM:
        return LONG_CLASS;
      case DATE_CLASS:
        return DATE_CLASS;
      default:
        return typeName;
    }
  }

  private static String genericFieldContentType(Field field) {
    return ((ParameterizedType) field.getGenericType()).getActualTypeArguments()[0].getTypeName();
  }

  public static String idField(Class<?> cls) {
    requireEntityType(cls);

    Field[] fields = cls.getDeclaredFields();
    for (Field field : fields) {
      if (isIdField(field)) {
        return field.getName();
      }
    }
    throw new PersistenceException("No Id field found for entity " + cls.getName());
  }

  public static <T> boolean isNewEntity(T entity) {
    Class<?> cls = entity.getClass();
    String idField = idField(cls);
    try {
      Field field = cls.getDeclaredField(idField);
      field.setAccessible(true);
      Object value = field.get(entity);
      Class<?> fieldType = field.getType();
      if (fieldType == boolean.class && Boolean.FALSE.equals(value)) {
        return true;
      }
      if (fieldType == char.class && (Character) value == 0) {
        return true;
      }
      if (fieldType.isPrimitive() && ((Number) value).doubleValue() == 0) {
        return true;
      }
      return value == null;
    } catch (NoSuchFieldException | IllegalAccessException e) {
      throw new PersistenceException("Error while checking Id field of entity", e);
    }
  }

  public static <T> String valueListString(T entity, boolean includeIdValue) {
    Class<?> cls = entity.getClass();
    StringBuilder sb = new StringBuilder();
    Arrays.stream(cls.getDeclaredFields())
        .filter(field -> isColumn(field) || isMemberRelationship(field))
        .filter(field -> includeIdValue || !isIdField(field))
        .forEach(field -> {
          field.setAccessible(true);
          Object value;
          try {
            value = field.get(entity);
          } catch (IllegalAccessException e) {
            throw new PersistenceException("Error while getting value list for saving entity", e);
          }
          if (isMemberRelationship(field) && value != null) {
            value = idValue(value);
          }
          if (DATE_CLASS.equals(fieldType(field)) && value != null) {
            value = new SimpleDateFormat("yyyy-MM-dd").format(value);
          }
          sb.append(toString(value)).append(", ");
        });
    String valuesList = sb.toString();
    return valuesList.substring(0, lastIndexOf(valuesList, ","));
  }

  public static <T> String columnValueListString(T entity, boolean includeIdValue) {
    Class<?> cls = entity.getClass();
    StringBuilder sb = new StringBuilder();
    Arrays.stream(cls.getDeclaredFields())
        .filter(field -> isColumn(field) || isMemberRelationship(field))
        .filter(field -> includeIdValue || !isIdField(field))
        .forEach(field -> {
          field.setAccessible(true);
          Object value;
          try {
            value = field.get(entity);
          } catch (IllegalAccessException e) {
            throw new PersistenceException(
                "Error while getting column and value list for saving entity", e);
          }
          if (isMemberRelationship(field) && value != null) {
            value = idValue(value);
          }
          sb.append(columnName(field)).append(" = ").append(toString(value)).append(", ");
        });
    String valuesList = sb.toString();
    return valuesList.substring(0, lastIndexOf(valuesList, ","));
  }

  private static String toString(Object value) {
    if (value == null) {
      return "NULL";
    }
    return "'" + value + "'";
  }

  public static <T> long idValue(T entity) {
    Class<?> cls = entity.getClass();
    try {
      Field field = cls.getDeclaredField(idField(cls));
      field.setAccessible(true);
      return field.getLong(entity);
    } catch (NoSuchFieldException | IllegalAccessException e) {
      throw new PersistenceException("Error while getting id value for entity " + cls.getName());
    }
  }

  public static <T> String idColumnValue(T entity) {
    Class<?> cls = entity.getClass();
    return idColumn(cls) + " = " + idValue(entity);
  }

  public static <T> Field foreignKeyField(Class<T> tClass, String foreignKeyColumnName) {
    return Arrays.stream(tClass.getDeclaredFields())
        .filter(ClassUtils::isColumn)
        .filter(field -> field.getAnnotation(Column.class).value().equals(foreignKeyColumnName))
        .findAny()
        .orElseThrow(() -> new PersistenceException("Error while identifying foreign key field "
            + foreignKeyColumnName + " for entity " + tClass.getName()));
  }

  public static <T> void setEntityId(T entity, long id) {
    Class<?> cls = entity.getClass();
    try {
      Field idField = cls.getDeclaredField(idField(cls));
      idField.setAccessible(true);
      idField.set(entity, id);
    } catch (NoSuchFieldException | IllegalAccessException e) {
      throw new PersistenceException("Error while updating Id field for entity " + cls.getName());
    }
  }

  public static String getNativeQuery(Class<?> tClass) {
    if (tClass.isAnnotationPresent(Table.class) && tClass.isAnnotationPresent(Query.class)) {
      return tClass.getAnnotation(Query.class).value();
    }
    return null;
  }

  public static boolean isLazyLoading(Field field) {
    if (field.isAnnotationPresent(OneToOne.class)
        && Loading.LAZY == field.getAnnotation(OneToOne.class).loading()) {
      return true;
    }
    if (field.isAnnotationPresent(OneToMany.class)
        && Loading.LAZY == field.getAnnotation(OneToMany.class).loading()) {
      return true;
    }
    if (field.isAnnotationPresent(ManyToOne.class)
        && Loading.LAZY == field.getAnnotation(ManyToOne.class).loading()) {
      return true;
    }

    return false;
  }

  public static boolean isCascadeDelete(Field field) {
    return (field.isAnnotationPresent(OneToMany.class)
        && field.getAnnotation(OneToMany.class).onDelete() == OnDelete.CASCADE) ||
        (field.isAnnotationPresent(OneToOne.class)
            && field.getAnnotation(OneToOne.class).onDelete() == OnDelete.CASCADE);
  }

  private static boolean isColumn(Field field) {
    return field.isAnnotationPresent(Column.class);
  }

  private static boolean isMemberRelationship(Field field) {
    return field.isAnnotationPresent(OneToOne.class)
        || field.isAnnotationPresent(ManyToOne.class);
  }

  private static boolean isCollectionRelationship(Field field) {
    return field.isAnnotationPresent(OneToMany.class);
  }

  private static boolean isIdField(Field field) {
    return field.isAnnotationPresent(Id.class);
  }
}
