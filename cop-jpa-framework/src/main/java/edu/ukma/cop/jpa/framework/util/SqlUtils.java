package edu.ukma.cop.jpa.framework.util;

import static edu.ukma.cop.jpa.framework.util.ClassUtils.columnListString;
import static edu.ukma.cop.jpa.framework.util.ClassUtils.columnValueListString;
import static edu.ukma.cop.jpa.framework.util.ClassUtils.idColumn;
import static edu.ukma.cop.jpa.framework.util.ClassUtils.idColumnValue;
import static edu.ukma.cop.jpa.framework.util.ClassUtils.tableName;
import static edu.ukma.cop.jpa.framework.util.ClassUtils.valueListString;

public class SqlUtils {

  private SqlUtils() {
    //Util class
  }

  public static <T> String createSelectByIdQuery(long id, Class<T> tClass) {
    return "select " + columnListString(tClass, true) + " from " + tableName(tClass)
        + " where " + idColumn(tClass) + "=" + id + ";";
  }

  public static <T> String createSelectAll(Class<T> tClass) {
    return "select " + columnListString(tClass, true)
        + " from " + tableName(tClass) + ";";
  }

  public static <T> String createSelectByFieldValue(String field, Object value, Class<T> tClass) {
    return "select " + columnListString(tClass, true)
        + " from " + tableName(tClass) + " where " + field + " = '" + value + "';";
  }

  public static <T> String createInsert(T entity) {
    Class<?> cls = entity.getClass();
    return "insert into " + tableName(cls) + " (" + columnListString(cls, false)
        + ") values (" + valueListString(entity, false) + ");";

  }

  public static <T> String createUpdate(T entity) {
    Class<?> cls = entity.getClass();
    return "update " + tableName(cls) + " set " + columnValueListString(entity, true)
        + " where " + idColumnValue(entity);
  }

  public static <T> String createDeleteById(long id, Class<T> tClass) {
    return "delete from " + tableName(tClass) + " where " + idColumn(tClass) + " = " + id;
  }
}
